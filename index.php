<?php
require_once "vendor/autoload.php";

use Framework\Routing\Request;
use Framework\Routing\Router;

echo (new Router(new Request()))->getContent();
